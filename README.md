# RogueNetwork

## What is RogueNetwork?
RogueNetwork is a PvP network.

## Do you have any public projects?
No, apart from this readme. We keep most our plugins private to maintain sale value if we wish to sell any of our plugins.

## What private projects do you have?
- MineHQ Bunkers remake
- Kohi SG remake
- Completely custom factions system and HCF core
- Hub plugin
- Network framework
- Essentials remake
- License Management system

Thanks,
subbotted